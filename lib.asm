global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error

%define MIN 0x30
%define MAX 0x39
%define EXIT 60
%define STDOUT 1
%define SYS_CALL 1
%define TWO 2

section .text

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax,rax ;i=0
    .loop: ;while(str[i]!=0) inc
        cmp byte[rax+rdi],0 ;if end -> go .end
        je .end
        inc rax ;i++
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov  rdx, rax
    mov  rax, SYS_CALL
    mov  rdi, STDOUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n' ;load "\n" and call print_char
    

; Принимает код символа и выводит его в stdout
print_char: 
    push rdi ;кладем код символа в память,так как системный вызов берет адрес в rsi
    mov rdx, 1     
    mov rsi, rsp ;берем адрес из стека
    pop rdi
    mov rax, 1      ; вызов "Вывод"
    mov rdi, 1      ; stdout
    syscall         
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r12
    push r13
    push r14
    sub rsp, 20
    mov r12, rsp
    xor r13, r13
    mov rax, rdi
    mov r8, 10
    
    .loop:
        xor rdx, rdx
        div r8
        add dl, 0x30
        lea r14, [r12 + r13]
        mov byte[r14], dl
        inc r13
        test rax,rax
        je .print
        jmp .loop

    .print:
    	dec r13
        cmp r13, 0
        jl .stop
        xor rdi, rdi
        lea r14, [r12 + r13]
        mov dil, byte[r14]
        call print_char
        jmp .print
       
    .stop:
        add rsp, 20
        pop r14
        pop r13
        pop r12
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    test rdi, rdi
    jns print_uint
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx				; индекс в данный момент
    xor rax, rax
    
    .loop:
        mov al, byte[rdi + rcx]
        cmp al, byte[rsi + rcx]      		
        jne .ret_false				
        test al, al				
        je .ret_true				
        inc rcx				
        jmp .loop

    .ret_false:
        xor rax,rax
        ret

    .ret_true:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rax		; make buffer
    mov rdi, 0
    mov rsi, rsp	; put address in stack
    mov rdx, 1
    syscall		; read
    
    cmp rax, -1	; проверяем прочитали ли символ
    je .ret_zero
    pop rax
    ret
    
    .ret_zero:
        pop rax
        xor rax, rax
        ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rdx, rdx
    xor rax, rax
    .loop:
        push rsi
        push rdi
        push rdx
        call read_char
        pop rdx
        pop rdi
        pop rsi

 	test rax, rax
        je .end
        cmp rax, ' '
        je .space
        cmp rax, '\n'
        je .space
        cmp rax, `\t`
        je .space

        mov  byte[rdi + rdx], al
        inc rdx
        cmp rdx, rsi 
        jge .overflow
        jmp .loop
    .space:
        test rdx, rdx
        jne .end
        jmp .loop
    .overflow:
        xor rax, rax
        xor rdx, rdx
        ret
    .end:
        mov byte[rdi + rdx], 0
        mov rax, rdi
        ret

    
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx	; индекс символа
    xor r8, r8		; будет находится анализируемый символ
    
    .loop:
        mov r8b, byte[rdi + rcx]
        cmp r8, MIN			; проверяем является ли символ -
        jb .stop			; символом цифры
        cmp r8, MAX			; цифры в таблице ASCII с номерами 0x30 - 0x39
        ja .stop
        
        and r8b, 0xf			; превращаем символ в цифру
        mov r9, 10
        mul r9				; добавляем разряд 
        add rax, r8			; rax = rax * 10 + r8
        
        inc rcx			; переходим к следующему символу
    	jmp .loop
    	
    .stop:
    	mov rdx, rcx
    	
    ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .negative
    cmp byte[rdi], '+'
    je .positive
    jmp parse_uint
    .positive:
        inc rdi
        call parse_uint
	inc rdx
        ret
    .negative:
        inc rdi
        call parse_uint
        inc rdx
        neg rax
        ret 


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy: ;args: rdi, rsi, rdx
    xor rax, rax
    xor rcx, rcx ;i=0
    push rsi
    push rdi
    push rcx
    push rdx
    call string_length
    xor rcx, rcx
    pop rdx
    pop rcx
    pop rdi
    pop rsi

    mov rax, rax ;rax=len(str)
    cmp rdx, rax 
    jl .error
    .loop:
        cmp rcx, rax;if(i> len(str)) break;
        jg .end
        mov r10,[rdi+rcx] ;buffer=string
        mov [rsi+rcx], r10
        inc rcx; inc
        jmp .loop
    
    .error:
        xor rax,rax ;return 0
        ret
    .end:
        mov rax, rax ;return lenght
        ret

; Принимает указатель на нуль-терминированную строку и ее длинну, печатает её в stderr
print_error:
    call string_length
    
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, TWO
    syscall
    
    jmp print_newline
    ret
