extern string_equals
global find_word

; Принимает указатель на нуль-терминированную строку
; и указатель на начало словаря. Возвращает адрес
; начала найденного элемента или 0 в случае его отсутствия

section .text
find_word:
    push r12
    push r13
    mov r12, rdi; указатель на строку ключа
    mov r13, rsi; рассматриваемый элемент словаря
        
    .loop:
        mov rdi, r12;
        lea rsi, [r13+8]	; адрес ключа
        call string_equals	; сравниваем ключи
        cmp rax, 1
        je .success
        mov rax, [r13]
        test rax, rax
        je .fail
        mov r13, rax
        jmp .loop
            
    .fail:
        xor rax, rax
        jmp .return
            
    .success:
        mov rax, r13
            
    .return:
        pop r13
        pop r12
        ret
