%include "lib.inc"
%include "words.inc"

section .bss
    buffer: times 255 db 0x0
    buffer_size: dq 255

section .rodata
    input_err_msg: db "Error: строка слишком длинная"
    find_err_msg: db "Error: введенного ключа не существует"


section .text
global _start
_start:
    mov rdi, buffer		
    mov rsi, [buffer_size]
    call read_word		; записываем пользовательский ввод в буфер
    
    test rax, rax			; проверяем получилось ли заполнить буфер
    je .input_error
    
    push rdx			;сохраняем  длинну строки
    mov rdi, buffer
    mov rsi, first_word
    call find_word
    pop rdx
    
    test rax, rax
    je .find_error
    
    lea rdi, [rax+rdx+9] 	; находим адрес указателя на строку значения
    call print_string
    call print_newline
    
    .input_error:
        mov rdi, input_err_msg
        call print_error
        call exit
    
    .find_error:
	mov rdi, find_err_msg
        call print_error
        call exit
        
