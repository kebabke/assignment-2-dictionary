ASM=nasm
FLAGS=-f elf64
LD = ld


.PHONY: clean

main: main.o dict.o lib.o
	$(LD) -o $@ $^

lib.o: lib.asm
	$(ASM) $(FLAGS) -o $@ $^

dict.o: dict.asm
	$(ASM) $(FLAGS) -o $@ $^

main.o: main.asm lib.inc
	$(ASM) $(FLAGS) -o $@ main.asm
        

clean:
	rm *.o
