%macro colon 2
; %1 - key
; %2 - label

%define key %1
%define label %2
	
%ifstr key
	%ifid label
		%ifdef prev
			label: dq prev
		%else
			label: dq 0
		%endif
		db key, 0x0
		%xdefine prev label
	%else
		%error "%2 -- !!!label!!!"
	%endif
%else 
	%error "key-- !!!string!!!"
%endif
%endmacro
